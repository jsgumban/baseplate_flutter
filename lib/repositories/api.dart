import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

final api = 'http://192.168.1.6:8080/v1';
final clientId = 'client_id';
final String clientSecret = 'client_secret';
final String auth =
    'Basic ${base64Encode(utf8.encode('$clientId:$clientSecret'))}';

class ApiRepository {
  Future<String> login({
    @required String email,
    @required String password,
  }) async {
    final url = '${api}/auth/login';
    final http.Response response = await http.post(
      url,
      headers: {
        'Authorization': auth,
        'Content-type': 'application/json',
      },
      body: jsonEncode({
        'email': email,
        'password': password,
      }),
    );

    var res = jsonDecode(response.body);

    if (res['fail'] != null) {
      throw res;
    }

    return res['data']['token'];
  }

  Future<String> register({
    @required String email,
    @required String password,
    @required String firstName,
    @required String lastName,
  }) async {
    final url = '${api}/auth/register';
    final http.Response response = await http.post(
      url,
      headers: {
        'Authorization': auth,
        'Content-type': 'application/json',
      },
      body: jsonEncode({
        'email': email,
        'password': password,
        'firstName': firstName,
        'lastName': lastName
      }),
    );

    var res = jsonDecode(response.body);
    if (res['fail'] != null) {
      throw res;
    }

    return res['data']['token'];
  }

  Future<void> deleteToken() async {
    /// delete from keystore/keychain
    await Future.delayed(Duration(seconds: 1));
    return;
  }

  Future<void> persistToken(String token) async {
    /// write to keystore/keychain
    await Future.delayed(Duration(seconds: 1));
    return;
  }

  Future<bool> hasToken() async {
    /// read from keystore/keychain
    await Future.delayed(Duration(seconds: 1));
    return false;
  }
}
