part of 'register_bloc.dart';

@immutable
abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}

class RegisterButtonPressed extends RegisterEvent {
  final String email;
  final String password;
  final String firstName;
  final String lastName;

  const RegisterButtonPressed({
    @required this.email,
    @required this.password,
    @required this.firstName,
    @required this.lastName,
  });

  @override
  List<Object> get props => [email, password, firstName, lastName];
}
