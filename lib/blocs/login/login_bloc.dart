import 'dart:async';
import 'package:baseplateflutter/blocs/authentication/authentication_bloc.dart';
import 'package:baseplateflutter/repositories/api.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final ApiRepository apiRepository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({
    @required this.apiRepository,
    @required this.authenticationBloc,
  });

  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginLoading();

      try {
        final token = await apiRepository.login(
          email: event.email,
          password: event.password,
        );

        authenticationBloc.add(LoggedIn(token: token));
        yield LoginInitial();
      } catch (err) {
        final String message = err['message'];
        yield LoginFailure(error: message.toString());
      }
    }
  }
}
