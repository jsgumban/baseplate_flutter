import 'package:baseplateflutter/blocs/authentication/authentication_bloc.dart';
import 'package:baseplateflutter/components/loadin_indicator.dart';
import 'package:baseplateflutter/pages/home_page.dart';
import 'package:baseplateflutter/pages/login_page.dart';
import 'package:baseplateflutter/pages/register_page.dart';
import 'package:baseplateflutter/pages/splash_page.dart';
import 'package:baseplateflutter/repositories/api.dart';
import 'package:baseplateflutter/utils/simple_bloc_delegate.dart';
import 'package:flutter/material.dart';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final apiRepository = ApiRepository();
  runApp(
    BlocProvider<AuthenticationBloc>(
      create: (context) {
        return AuthenticationBloc(apiRepository: apiRepository)
          ..add(AppStarted());
      },
      child: App(apiRepository: apiRepository),
    ),
  );
}

class App extends StatelessWidget {
  final ApiRepository apiRepository;

  App({Key key, @required this.apiRepository});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        // ignore: missing_return
        builder: (context, state) {
          if (state is AuthenticationUninitialized) {
            return SplashPage();
          }
          if (state is AuthenticationAuthenticated) {
            return HomePage();
          }
          if (state is AuthenticationUnauthenticated) {
            return MaterialApp(
              initialRoute: LoginPage.id,
              routes: {
                LoginPage.id: (context) {
                  return LoginPage(apiRepository: apiRepository);
                },
                RegisterPage.id: (context) {
                  return RegisterPage(apiRepository: apiRepository);
                }
              },
            );
          }
          if (state is AuthenticationLoading) {
            return LoadingIndicator();
          }
        },
      ),
    );
  }
}
