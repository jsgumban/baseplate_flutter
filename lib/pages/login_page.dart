import 'package:baseplateflutter/blocs/authentication/authentication_bloc.dart';
import 'package:baseplateflutter/blocs/login/login_bloc.dart';
import 'package:baseplateflutter/components/login_form.dart';
import 'package:baseplateflutter/pages/register_page.dart';
import 'package:baseplateflutter/repositories/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatelessWidget {
  static const String id = 'login_page';
  final ApiRepository apiRepository;

  LoginPage({Key key, @required this.apiRepository});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: BlocProvider(
        create: (context) {
          return LoginBloc(
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
            apiRepository: apiRepository,
          );
        },
        child: LoginForm(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Text('R'),
        onPressed: () {
          Navigator.pushNamed(context, RegisterPage.id);
        },
      ),
    );
  }
}
