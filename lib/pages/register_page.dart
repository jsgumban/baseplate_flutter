import 'package:baseplateflutter/blocs/authentication/authentication_bloc.dart';
import 'package:baseplateflutter/blocs/login/login_bloc.dart';
import 'package:baseplateflutter/blocs/register/register_bloc.dart';
import 'package:baseplateflutter/components/login_form.dart';
import 'package:baseplateflutter/components/register_form.dart';
import 'package:baseplateflutter/pages/login_page.dart';
import 'package:baseplateflutter/repositories/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterPage extends StatelessWidget {
  static const String id = 'register_page';

  final ApiRepository apiRepository;

  RegisterPage({Key key, @required this.apiRepository});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: BlocProvider(
        create: (context) {
          return RegisterBloc(
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
            apiRepository: apiRepository,
          );
        },
        child: RegisterForm(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Text('L'),
        onPressed: () {
          Navigator.pushReplacementNamed(context, LoginPage.id);
        },
      ),
    );
  }
}
